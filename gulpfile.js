var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    sass        = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer'),
    sourcemaps  = require('gulp-sourcemaps'),
    jade        = require('gulp-jade'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant'),
    concat      = require('gulp-concat');


gulp.task('browser-sync', ['sass'], function() {
    browserSync.init({
        server: {
            baseDir: 'build'
        },
        notify: false
    });
});


gulp.task('jade', function(){
  return gulp.src('components/jade/*.jade')
  .pipe(jade({
    pretty: true
  }))
  .pipe(gulp.dest('build'))
});


gulp.task('sass', function () {
  return gulp.src('components/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))

      .pipe(sass({
        outputStyle: 'compressed',
        onError: browserSync.notify
      }))
      .pipe(prefix(['last 15 versions', '> 1%', 'ie 9'], { cascade: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build/css'))
    .pipe(browserSync.reload({stream:true}))
});


gulp.task('imagemin', () => {
    return gulp.src('components/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('build/images'));
});


gulp.task('javascript', function() {
  return gulp.src(['bower_components/jquery/dist/jquery.js', 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js', 'components/js/main.js'])
    .pipe(sourcemaps.init())
      .pipe(concat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'));
});


gulp.task('watch', function () {
    gulp.watch('components/scss/**', ['sass']);
    gulp.watch('components/jade/**', ['jade']);
    gulp.watch('components/js/**', ['javascript']);
    gulp.watch("build/*.html").on('change', browserSync.reload);
});


gulp.task('default', ['browser-sync', 'imagemin', 'watch']);

$(document).ready(function(){
    $("#feed a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
    });
});

$(document).ready(function(){
    $("#groupMembers a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
    });
});

$('.group__icon--members').click(function(){
  var status = $('#group__tabs--members').hasClass('is-close');
  if(status) {
    {$('#group__tabs--members').removeClass('is-close');}
  }
  else $('#group__tabs--members').addClass('is-close');
});

$('.group__icon--options').click(function(){
  var status = $('#group__inner--options').hasClass('is-close');
  if(status) {
    {$('#group__inner--options').removeClass('is-close');}
  }
  else $('#group__inner--options').addClass('is-close');
});

